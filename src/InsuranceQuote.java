import static org.junit.Assert.assertEquals;

import java.util.Random;
import java.util.Scanner;

import org.junit.Test;;



public class InsuranceQuote {
	public static void main(String[] args) {
	
		/*System.out.println("Create a new Customer");
		String cust_Id = null;
		String name = null;
		String gender = null;
		int age = 0;
		CurrentHealth cH = new CurrentHealth();
		Habits habit = new Habits();
		
		CreateCustomer createCustomer = new CreateCustomer();
		Scanner s = new Scanner(System.in);
		System.out.println("Please Enter Customer Name");
		//Random rand = new Random(); 
		
		
		createCustomer.setName( s.nextLine());
		
		
		System.out.println("Please Enter Customer Gender");
		createCustomer.setGender((s.nextLine()));
		System.out.println("Please Enter Customer Age");
		createCustomer.setAge(s.nextInt());
		System.out.println("Is Customer having Blood Pressure Problem");
		cH.setBloodPressure(s.nextBoolean());
		System.out.println("Is Customer having Blood Sugar Problem");
		cH.setBloodSugar(s.nextBoolean());
		System.out.println("Is Customer having Hyper Tension Problem");
		cH.setHyperTension(s.nextBoolean());
		System.out.println("Is Customer OverWeight");
		cH.setOverweight(s.nextBoolean());
		createCustomer.setcH(cH);
		System.out.println("Does the Customer drink alcohol");
		habit.setAlcohol(s.nextBoolean());
		System.out.println("Does the Customer exercise daily");
		habit.setDailyExercises(s.nextBoolean());
		System.out.println("Does the Customer have drugs");
		habit.setDrugs(s.nextBoolean());
		System.out.println("Does the Customer Smoke");
		habit.setSmoking(s.nextBoolean());
		createCustomer.setHabit(habit);
		
		InsuranceQuote insQuote1 = new InsuranceQuote();
		System.out.println(insQuote1.calculateInsurance(createCustomer));*/
		
		InsuranceQuote insQuote = new InsuranceQuote();
		insQuote.testCalculatePremium();
		
		
	}
	
	@Test
	   public void testCalculatePremium() {

		CreateCustomer createCustomer = new CreateCustomer();
		Scanner s = new Scanner(System.in);
		CurrentHealth cH = new CurrentHealth();
		Habits habit = new Habits();

		createCustomer.setName("Vibhu");
		System.out.println("Please Enter Customer Gender");
		createCustomer.setGender("Male");
		System.out.println("Please Enter Customer Age");
		createCustomer.setAge(17);
		System.out.println("Is Customer having Blood Pressure Problem");
		cH.setBloodPressure(false);
		System.out.println("Is Customer having Blood Sugar Problem");
		cH.setBloodSugar(false);
		System.out.println("Is Customer having Hyper Tension Problem");
		cH.setHyperTension(false);
		System.out.println("Is Customer OverWeight");
		cH.setOverweight(false);
		createCustomer.setcH(cH);
		System.out.println("Does the Customer drink alcohol");
		habit.setAlcohol(false);
		System.out.println("Does the Customer exercise daily");
		habit.setDailyExercises(false);
		System.out.println("Does the Customer have drugs");
		habit.setDrugs(false);
		System.out.println("Does the Customer Smoke");
		habit.setSmoking(false);
		createCustomer.setHabit(habit);
		
		InsuranceQuote insQuote = new InsuranceQuote();
		
			
	      double premium = insQuote.calculateInsurance(createCustomer);
	      assertEquals(5100,premium,0.0);
	   }
	
	public double calculateInsurance(CreateCustomer createCustomer)
	{
		double basePremium = 5000;
		if(createCustomer.getAge() < 18)
			basePremium = 5000;
		else if(createCustomer.getAge() >= 18 && createCustomer.getAge() < 25)
			basePremium = basePremium + basePremium * 0.1;
		else if(createCustomer.getAge() >= 25 && createCustomer.getAge() < 30)
			basePremium = basePremium + basePremium * 0.1*0.1;
		else if(createCustomer.getAge() >= 30 && createCustomer.getAge() < 35)
			basePremium = basePremium + basePremium * 0.1*0.1*0.1;
		else if(createCustomer.getAge() >= 35 && createCustomer.getAge() < 40)
			basePremium = basePremium + basePremium * 0.1*0.1*0.1*0.1;
		else if (createCustomer.getAge() > 40)
			basePremium = basePremium + basePremium * 0.1*0.1*0.1*0.1*0.1;
		
		if(createCustomer.getGender().equalsIgnoreCase("Male"))
			basePremium = basePremium + 0.02 * basePremium;
		
		if(createCustomer.getcH().isBloodSugar() == true)
		{
			basePremium = basePremium + basePremium * 0.1;
		}
		if(createCustomer.getcH().isBloodPressure() == true)
		{
			basePremium = basePremium + basePremium * 0.1;
		}
		if(createCustomer.getcH().isHyperTension() == true)
		{
			basePremium = basePremium + basePremium * 0.1;
		}
		if(createCustomer.getcH().isOverweight() == true)
		{
			basePremium = basePremium + basePremium * 0.1;
		}
				
		if(createCustomer.getHabit().isDailyExercises()== true)
		{
			basePremium = basePremium - basePremium * 0.03;
		}
		if(createCustomer.getHabit().isAlcohol() == true)
		{
			basePremium = basePremium + basePremium * 0.03;
		}
		if(createCustomer.getHabit().isDrugs() == true)
		{
			basePremium = basePremium + basePremium * 0.03;
		}
		if(createCustomer.getHabit().isSmoking()== true)
			
		{
			basePremium = basePremium + basePremium * 0.03;
		}
		return basePremium ;
	}
	
	
	
	
}
