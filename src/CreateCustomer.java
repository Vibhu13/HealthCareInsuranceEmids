import java.util.Random;
import java.util.Scanner;

public class CreateCustomer {

	String cust_Id = null;
	String name = null;
	String gender = null;
	int age = 0;
	CurrentHealth cH = new CurrentHealth();
	Habits habit = new Habits();
	
	public String getCust_Id() {
		return cust_Id;
	}

	public void setCust_Id(String cust_Id) {
		this.cust_Id = cust_Id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public CurrentHealth getcH() {
		return cH;
	}

	public void setcH(CurrentHealth cH) {
		this.cH = cH;
	}

	public Habits getHabit() {
		return habit;
	}

	public void setHabit(Habits habit) {
		this.habit = habit;
	}
	
	
	
	


}
