
public class Habits {
	
	boolean smoking = false;
	boolean alcohol = false;
	boolean dailyExercises = false;
	boolean drugs = false;
	
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isAlcohol() {
		return alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}
	public boolean isDailyExercises() {
		return dailyExercises;
	}
	public void setDailyExercises(boolean dailyExercises) {
		this.dailyExercises = dailyExercises;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	
	
}
